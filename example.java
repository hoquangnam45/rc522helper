package com.iot.hoquangnam.lab3iot;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.galarzaa.androidthings.Rc522;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.SpiDevice;
import com.google.android.things.pio.PeripheralManager;

import java.io.IOException;

/**
 * Skeleton of an Android Things activity.
 * <p>
 * Android Things peripheral APIs are accessible through the class
 * PeripheralManagerService. For example, the snippet below will open a GPIO pin and
 * set it to HIGH:
 *
 * <pre>{@code
 * PeripheralManagerService service = new PeripheralManagerService();
 * mLedGpio = service.openGpio("BCM6");
 * mLedGpio.setDirection(Gpio.DIRECTION_OUT_INITIALLY_LOW);
 * mLedGpio.setValue(true);
 * }</pre>
 * <p>
 * For more complex peripherals, look for an existing user-space driver, or implement one if none
 * is available.
 *
 * @see <a href="https://github.com/androidthings/contrib-drivers#readme">https://github.com/androidthings/contrib-drivers#readme</a>
 */
public class MainActivity extends Activity {
    private static String TAG = "rfid";
    private Rc522Helper mRc522;
    private SpiDevice spiDevice;
    private Gpio resetPin;

    private  Handler mHandler = new Handler();

    // Sector trailer access
    private Access ReadKeyA;
    private Access ReadKeyB;
    private Access WriteKeyA;
    private Access WriteKeyB;
    private Access ReadAccessBit;
    private Access WriteAccessBit;
    // Block access
    private Access ReadBlock;
    private Access WriteBlock;
    private Access IncrementBlock;
    private Access DecrementBlock;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        PeripheralManager pioService = PeripheralManager.getInstance();
        try {
            /* Names based on Raspberry Pi 3 */
            spiDevice = pioService.openSpiDevice("SPI0.0");
            resetPin = pioService.openGpio("BCM25");
            mRc522 = new Rc522Helper(spiDevice, resetPin);
        } catch (IOException e) {
            e.printStackTrace();
        }
        boolean ret;
        Log.d(TAG, "**************************start writing********************************8");
        if (mRc522.WaitRfid(10000)) {
            ret = mRc522.WriteBlock(Rc522.AUTH_A, Rc522Helper.FactorykeyA, (byte) 2, (byte) 0, mRc522.StringToByteArray("Ho Quang Nam"));
            Log.d(TAG, "1 " + ret);
            ret = mRc522.WriteBlock(Rc522.AUTH_A, Rc522Helper.FactorykeyA, (byte) 2, (byte) 1, mRc522.StringToByteArray("11/04/1997"));
            Log.d(TAG, "2 " + ret);
            ret = mRc522.WriteBlock(Rc522.AUTH_A, Rc522Helper.FactorykeyA, (byte) 2, (byte) 2, mRc522.StringToByteArray("1512065"));
            Log.d(TAG, "3 " + ret);
            mRc522.stopCrypto();
        }
        mHandler.post(new Runnable() {
            @Override
            public void run(){
                byte[] buffer = new byte[16];
                boolean ret;
                if (!mRc522.WaitRfid(10000)) return;
                ret = mRc522.ReadBlock(Rc522.AUTH_A, Rc522Helper.FactorykeyA, (byte)2, (byte)0, buffer);
                Log.d(TAG, "4 "+ret);
                Log.d(TAG, mRc522.ByteArrayToString(buffer));
                ret = mRc522.ReadBlock(Rc522.AUTH_A, Rc522Helper.FactorykeyA, (byte)2, (byte)1, buffer);
                Log.d(TAG, "5 "+ret);
                Log.d(TAG, mRc522.ByteArrayToString(buffer));
                ret = mRc522.ReadBlock(Rc522.AUTH_A, Rc522Helper.FactorykeyA, (byte)2, (byte)2, buffer);
                Log.d(TAG, "6 "+ret);
                Log.d(TAG, mRc522.ByteArrayToString(buffer));
                mRc522.stopCrypto();
                mHandler.postDelayed(this, 1000);
            }
        });
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        try{
            if(spiDevice != null){
                spiDevice.close();
            }
            if(resetPin != null){
                resetPin.close();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
