Download the original library: https://github.com/Galarzaa90/android-things-rc522. This add a few easy to use method to the original class to use to read and write data to data, some included method such as update key, write sector trailer, translate access to access bit

# If you want to read or write only one block, please use ReadRfid or WriteRfid
# If you want to read multiple block, please use WaitRfid first and use ReadBlock or WriteBlock and then StopCrypto after you had done reading and writing