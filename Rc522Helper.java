package com.iot.hoquangnam.lab3iot;

import com.galarzaa.androidthings.Rc522;
import com.google.android.things.pio.Gpio;
import com.google.android.things.pio.SpiDevice;

import java.io.IOException;

enum Access{
    BY_KEYA, BY_KEYB, BY_NONE, BY_BOTH
}
public class Rc522Helper extends Rc522{
    private static SpiDevice dummydevice;
    private static Gpio dummyresetPin;

    public static final byte[] DEFAULT_SECTOR_TRAILER = {(byte)0xFF, (byte)0x07, (byte)0x80};

    public static final byte[] FactorykeyA = {(byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF};
    public static final byte[] FactorykeyB = {(byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF, (byte)0xFF};

    private Rc522Helper() throws IOException {
        super(dummydevice, dummyresetPin);
    }
    public Rc522Helper(SpiDevice device, Gpio resetPin) throws IOException {
        super(device, resetPin);
    }
    /*-----------------------------------------------*/
    //----- Assume byte array is right aligned ------//
    public byte[] StringToByteArray(String content){
        byte [] ret = new byte[16];
        if (content.length() > 16) return ret;
        System.arraycopy(content.getBytes(), 0, ret,16-content.length(), content.length());
        return ret;
    }
    public String ByteArrayToString(byte[] content){
        int i;
        for(i = 0; i < 16; i++)
            if (content[i] != 0) break;
        byte buffer[] = new byte[16-i];
        System.arraycopy(content, i,  buffer, 0, 16-i);
        return new String(buffer);
    }
    /************************************************/
    public boolean ReadBlock(byte AUTH_mode, byte[] key, byte sector, byte block, byte[] buffer){
        // Get the address of the desired block
        byte blockAddr = Rc522.getBlockAddress(sector, block);
        //We need to authenticate the card, each sector can have a different key
        boolean result = super.authenticateCard(AUTH_mode, blockAddr, key);
        if (!result) return false;
        result = super.readBlock(blockAddr, buffer);
        if (!result) return false;
        return true;
    }
    public boolean WriteBlock(byte AUTH_mode, byte[] key, byte sector, byte block, byte[] WriteData){
        // Get the address of the desired block
        byte blockAddr = Rc522.getBlockAddress(sector, block);
        //We need to authenticate the card, each sector can have a different key
        boolean result = super.authenticateCard(AUTH_mode, blockAddr, key);
        if (!result) return false;
        result = super.writeBlock(blockAddr, WriteData);
        if (!result) return false;
        return true;
    }
    public boolean WriteTrailer(byte AUTH_mode, byte[] Key, byte sector, byte[] KeyA, byte[] AccessBit, byte[] KeyB, byte UserData){
        // Get the address of the desired block
        byte blockAddr = Rc522.getBlockAddress(sector, 3);
        //We need to authenticate the card, each sector can have a different key
        boolean result = super.authenticateCard(AUTH_mode, blockAddr, Key);
        if (!result) return false;
        result = super.writeTrailer(sector, KeyA, AccessBit, UserData, KeyB);
        if (!result) return false;
        return true;
    }
    public boolean getSectorTrailer(byte AUTH_mode, byte[] Key, byte sector, byte[] buffer){
        byte [] content = new byte[16];
        boolean ret = ReadBlock(AUTH_mode, Key, sector, (byte) 3, content);
        if (!ret) return false;
        System.arraycopy(content,6, buffer, 0, 4);
        return true;
    }
    /*************************************************/

    private boolean readRfid(byte AUTH_mode, byte[] key, byte sector, byte block, byte[] buffer, int timeout){
        if(!WaitRfid(timeout)) return false;
        boolean ret = ReadBlock(AUTH_mode, key, sector, block, buffer);
        super.stopCrypto();
        if (!ret) return false;
        return true;
    }
    private boolean writeRfid(byte AUTH_mode, byte[] key, byte sector, byte block, byte[] WriteData, int timeout){
        if(!WaitRfid(timeout)) return false;
        boolean ret =  WriteBlock(AUTH_mode,key, sector,block, WriteData);
        super.stopCrypto();
        if (!ret) return false;
        return true;
    }
    public boolean UpdateKey(byte AUTH_mode, byte[] oldKey, byte[] keyA, byte[] keyB, byte sector, int timeout){
        if (!WaitRfid(timeout)) return false;
        byte[] SectorTrailer = new byte[4];
        boolean ret = getSectorTrailer(AUTH_mode, oldKey, sector, SectorTrailer);
        if (!ret) return false;
        byte[] AccessBit = new byte[3];
        byte UserData = SectorTrailer[3];
        System.arraycopy(SectorTrailer, 0, AccessBit, 0, 3);
        ret = WriteTrailer(AUTH_mode, oldKey, sector, keyA, AccessBit, keyB, UserData);
        super.stopCrypto();
        if (!ret) return false;
        return true;
    }

    ////////////////////////////////////////////////////////////////////

    ////////////////////////////////////////////////////////////////////
    public boolean WaitRfid(int Timeout){
        long startTime = System.currentTimeMillis();
        long endTime = startTime + Timeout;
        while(true){
            long currentTime = System.currentTimeMillis();
            if (currentTime > endTime) return false;
            boolean success = super.request();
            if(!success){
                continue;
            }
            success = super.antiCollisionDetect();
            if(!success){
                continue;
            }
            byte[] uid = super.getUid();
            super.selectTag(uid);
            return true;
        }
    }
    public boolean SetAccessSectorTrailer(Access ReadKeyA,
                                          Access WriteKeyA,
                                          Access ReadAccessBits,
                                          Access WriteAccessBits,
                                          Access ReadKeyB,
                                          Access WriteKeyB,
                                          byte[] BlockAccessBit){
        if (    (ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_KEYA) &&
                (ReadAccessBits == Access.BY_KEYA) &&
                (WriteAccessBits == Access.BY_NONE) &&
                (ReadKeyB == Access.BY_KEYA) &&
                (WriteKeyB == Access.BY_KEYA)){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 0;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_NONE) &&
                (ReadAccessBits == Access.BY_KEYA) &&
                (WriteAccessBits == Access.BY_NONE) &&
                (ReadKeyB == Access.BY_KEYA) &&
                (WriteKeyB == Access.BY_NONE)){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 0;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_KEYB) &&
                (ReadAccessBits == Access.BY_BOTH) &&
                (WriteAccessBits == Access.BY_NONE) &&
                (ReadKeyB == Access.BY_NONE) &&
                (WriteKeyB == Access.BY_KEYB)){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 0   ;
            BlockAccessBit[2] = 0;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_NONE) &&
                (ReadAccessBits == Access.BY_BOTH) &&
                (WriteAccessBits == Access.BY_NONE) &&
                (ReadKeyB == Access.BY_NONE) &&
                (WriteKeyB == Access.BY_NONE)){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 0;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_KEYA) &&
                (ReadAccessBits == Access.BY_KEYA) &&
                (WriteAccessBits == Access.BY_KEYA) &&
                (ReadKeyB == Access.BY_KEYA) &&
                (WriteKeyB == Access.BY_KEYA)){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 1;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_KEYB) &&
                (ReadAccessBits == Access.BY_BOTH) &&
                (WriteAccessBits == Access.BY_KEYB) &&
                (ReadKeyB == Access.BY_NONE) &&
                (WriteKeyB == Access.BY_KEYB)){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 1;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_NONE) &&
                (ReadAccessBits == Access.BY_BOTH) &&
                (WriteAccessBits == Access.BY_KEYB) &&
                (ReadKeyB == Access.BY_NONE) &&
                (WriteKeyB == Access.BY_NONE)){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 1;
        }
        else if ((ReadKeyA == Access.BY_NONE) &&
                (WriteKeyA == Access.BY_NONE) &&
                (ReadAccessBits == Access.BY_BOTH) &&
                (WriteAccessBits == Access.BY_NONE) &&
                (ReadKeyB == Access.BY_NONE) &&
                (WriteKeyB == Access.BY_NONE)){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 1;
        }
        else {
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 1;
            return false;
        }
        return true;
    }
    public boolean SetAccessDataBlock(Access Read,
                                      Access Write,
                                      Access Increment,
                                      Access Decrement,
                                      byte[] BlockAccessBit){
        if (Read == Access.BY_BOTH &&
                Write == Access.BY_BOTH &&
                Increment == Access.BY_BOTH &&
                Decrement == Access.BY_BOTH){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 0;
        }
        else if(Read == Access.BY_BOTH &&
                Write == Access.BY_NONE &&
                Increment == Access.BY_NONE &&
                Decrement == Access.BY_NONE){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 0;
        }
        else if(Read == Access.BY_BOTH &&
                Write == Access.BY_KEYB &&
                Increment == Access.BY_NONE &&
                Decrement == Access.BY_NONE){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 0;
        }
        else if(Read == Access.BY_BOTH &&
                Write == Access.BY_KEYB &&
                Increment == Access.BY_KEYB &&
                Decrement == Access.BY_BOTH){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 0;
        }
        else if(Read == Access.BY_BOTH &&
                Write == Access.BY_NONE &&
                Increment == Access.BY_NONE &&
                Decrement == Access.BY_BOTH){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 1;
        }
        else if(Read == Access.BY_KEYB &&
                Write == Access.BY_KEYB &&
                Increment == Access.BY_NONE &&
                Decrement == Access.BY_NONE){
            BlockAccessBit[0] = 0;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 1;
        }
        else if(Read == Access.BY_KEYB &&
                Write == Access.BY_NONE &&
                Increment == Access.BY_NONE &&
                Decrement == Access.BY_NONE){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 0;
            BlockAccessBit[2] = 1;
        }
        else if(Read == Access.BY_NONE &&
                Write == Access.BY_NONE &&
                Increment == Access.BY_NONE &&
                Decrement == Access.BY_NONE){
            BlockAccessBit[0] = 1;
            BlockAccessBit[1] = 1;
            BlockAccessBit[2] = 1;
        }
        else return false;
        return true;
    }
}
